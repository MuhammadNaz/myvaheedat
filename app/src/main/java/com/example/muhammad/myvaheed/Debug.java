package com.example.muhammad.myvaheed;

import android.util.Log;

/**
 * Created by Muhammad on 19.12.2015.
 */
public class Debug {

    private static final String TAG = "MyvaheedDebug";

    public static void log(String text) {
        Log.d(TAG, text);
    }

    public static void logError(String text) {
        Log.e(TAG, text);
    }
}
