package com.example.muhammad.myvaheed;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.util.List;
import java.util.Random;


public class Utils {

    public static final String TAG = "Utils";

    //ServiceTimeTracking|Cacheer|TrackController|User|DataDiagnosticController|PostData|Utils
	public static void log(String text) {
		Log.e("MyApp", text);
	}
	public static void log(String TAG, String text) {Log.e(TAG, text);}
	@SuppressWarnings("rawtypes")
	public static boolean isServiceRunning(Context context, Class serviceClass) {
	    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

    public static void sendSMS(final Context context, String phoneNumber, String message)
    {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        context.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(context, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    public synchronized static void saveDataSerializable(Object object, String path_file_name) {
        try {
            FileOutputStream outStream = new FileOutputStream(path_file_name);
            ObjectOutputStream objOutputStream = new ObjectOutputStream(outStream);
            objOutputStream.writeObject(object);
            objOutputStream.flush();
            objOutputStream.close();
            outStream.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "saveData File not found");
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
            Log.e(TAG, "saveData IOEXCEPTION");
        }
    }

    public synchronized static Object loadDataSerializable(String path_file_name)  {
        Object object = null;
        try {
            FileInputStream inStream = new FileInputStream(path_file_name);
            ObjectInputStream objInputStream = new ObjectInputStream(inStream);
            object = objInputStream.readObject();
            objInputStream.close();
            inStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "loadData file not foud = " + path_file_name);
        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "loadData IOEXCEPTION = " + path_file_name);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "loadData Class not found = " + path_file_name);
        }
        return object;
    }

    public static boolean isAvailableInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static void saveBitmap(Bitmap bitmap, String filename) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeString(String text, String path) {
        try {
            File file = new File(path);
            if (!file.exists())
                file.createNewFile();
            FileOutputStream out = new FileOutputStream(path);
            out.write(text.getBytes(), 0, text.getBytes().length);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static void deleteFile(String path_file_name) {
        File file = new File(path_file_name);
        if (file.exists()) {
            String deleteCmd = "rm -r " + (path_file_name);
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
                log(TAG, "Deleted file = " + path_file_name);
            } catch (IOException e) {
            }
        } else {
        }
    }
    /*public static void saveImageFromMat(String filename, Mat mat) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, filename);
        Boolean bool = null;
        filename = file.toString();
        bool = Highgui.imwrite(filename, mat);
        if (bool == true)
            Utils.log("SUCCESS writing image to external storage");
        else
            Utils.log("Fail writing image to external storage");
    }*/

    public static Bitmap getBitmapFromAsset(AssetManager assets, String fileName, Bitmap.Config config) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = config;
        options.inMutable = true;
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return bitmap;
    }

    public static ActivityManager.RunningAppProcessInfo getProcessInfo(String TAG, Context context) {
        final int PROCESS_STATE_TOP = 2;
        ActivityManager.RunningAppProcessInfo currentInfo = null;
        Field field = null;
        try {
            field = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");
        } catch (Exception e) {
            Utils.log(TAG, "Exception in getDeclaredField = " + e.getMessage());
        }
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appList = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo app : appList) {
            if (app.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                    app.importanceReasonCode == 0 ) {
                Integer state = null;
                try {
                    state = field.getInt( app );
                } catch (IllegalAccessException e) {
                    Utils.log(TAG, "IlleagalAccessException");
                }
                if (state != null && state == PROCESS_STATE_TOP) {
                    currentInfo = app;
                    break;
                }
            }
        }
        return currentInfo;
    }

    /*public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }*/

    public static void sendMsgToActivity(PendingIntent pendingIntent, Context context, int requestCode, Object ... obj) {
        Intent iResult = new Intent();
        for (int i = 0; i < obj.length; i+=2) {
            if (Integer.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(Integer) obj[i + 1]);
            if (String.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(String) obj[i + 1]);
            if (Float.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(Float) obj[i + 1]);
            if (Double.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(Double) obj[i + 1]);
            if (Byte.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(byte[]) obj[i + 1]);
            if (Boolean.class == obj[i + 1].getClass())
                iResult.putExtra((String)obj[i],(Boolean) obj[i + 1]);
        }
        try {
            pendingIntent.send(context, requestCode, iResult);
        } catch (PendingIntent.CanceledException e1) {
            Utils.log("Error for sending distance to activity" + e1.getMessage());
        }
    }

    public static JSONArray readFileToJsonArray(Context context, String filename) {
        BufferedReader reader = null;
        JSONArray jsonObj = new JSONArray();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(filename)));

            // do reading, usually loop until end of file reading
            String mLine = reader.readLine();
            while (mLine != null) {
                mLine = reader.readLine();
                jsonObj.put(mLine);
            }
        } catch (IOException e) {
            //log the exception
            Toast.makeText(context, "Error for reading file // jsonArray creating", Toast.LENGTH_SHORT).show();

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                    Toast.makeText(context, "Error for reading file // jsonArray creating", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return jsonObj;
    }

    public static void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {

        final int frameSize = width * height;

        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
            for (int i = 0; i < width; i++, yp++) {
                int y = (0xff & ((int) yuv420sp[yp])) - 16;
                if (y < 0)
                    y = 0;
                if ((i & 1) == 0) {
                    v = (0xff & yuv420sp[uvp++]) - 128;
                    u = (0xff & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                if (r < 0)
                    r = 0;
                else if (r > 262143)
                    r = 262143;
                if (g < 0)
                    g = 0;
                else if (g > 262143)
                    g = 262143;
                if (b < 0)
                    b = 0;
                else if (b > 262143)
                    b = 262143;

                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
            }
        }
    }


    public static void decodeYUV(int[] out, byte[] fg, int width, int height)
            throws NullPointerException, IllegalArgumentException {
        int sz = width * height;
        if (out == null)
            throw new NullPointerException("buffer out is null");
        if (out.length < sz)
            throw new IllegalArgumentException("buffer out size " + out.length
                    + " < minimum " + sz);
        if (fg == null)
            throw new NullPointerException("buffer 'fg' is null");
        if (fg.length < sz)
            throw new IllegalArgumentException("buffer fg size " + fg.length
                    + " < minimum " + sz * 3 / 2);
        int i, j;
        int Y, Cr = 0, Cb = 0;
        for (j = 0; j < height; j++) {
            int pixPtr = j * width;
            final int jDiv2 = j >> 1;
            for (i = 0; i < width; i++) {
                Y = fg[pixPtr];
                if (Y < 0)
                    Y += 255;
                if ((i & 0x1) != 1) {
                    final int cOff = sz + jDiv2 * width + (i >> 1) * 2;
                    Cb = fg[cOff];
                    if (Cb < 0)
                        Cb += 127;
                    else
                        Cb -= 128;
                    Cr = fg[cOff + 1];
                    if (Cr < 0)
                        Cr += 127;
                    else
                        Cr -= 128;
                }
                int R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5);
                if (R < 0)
                    R = 0;
                else if (R > 255)
                    R = 255;
                int G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1)
                        + (Cr >> 3) + (Cr >> 4) + (Cr >> 5);
                if (G < 0)
                    G = 0;
                else if (G > 255)
                    G = 255;
                int B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6);
                if (B < 0)
                    B = 0;
                else if (B > 255)
                    B = 255;
                out[pixPtr++] = 0xff000000 + (B << 16) + (G << 8) + R;
            }
        }

    }

    public static String getRandomEnglishWord(Context context, String filename) {
        JSONArray words = readFileToJsonArray(context, filename);
        try {
            return words.getString(new Random(System.currentTimeMillis()).nextInt(words.length()));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
