package com.example.muhammad.myvaheed.fragments;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammad.myvaheed.MyApp;
import com.example.muhammad.myvaheed.R;
import com.example.muhammad.myvaheed.bluetooth.DeviceData;
import com.example.muhammad.myvaheed.bluetooth.initiator.BluetoothController;

import java.util.ArrayList;

/**
 * Created by Muhammad on 24.09.2015.
 */
public class MyDialogFragment extends DialogFragment implements BluetoothController.IBluetoothController {

    ArrayList<DeviceData> listDevices = new ArrayList<>();
    ListView listView;
    Button btnFindDevices;
    BaseAdapter listAdapter;
    BluetoothController bluetoothController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bluetoothController = new BluetoothController(getContext(), this);
        // Pick a style based on the num.
        int style = DialogFragment.STYLE_NORMAL, theme = android.R.style.Theme_Holo_Light_Panel;
        setStyle(style, theme);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);
        listView = (ListView) v.findViewById(R.id.lvDevices);
        btnFindDevices = (Button) v.findViewById(R.id.btnFindDevices);

        btnFindDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothController.discoverDevices();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bluetoothController.connect(listDevices.get(position).device);
            }
        });
        listAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return listDevices.size();
            }

            @Override
            public Object getItem(int position) {
                return listDevices.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
                ((TextView) convertView.findViewById(android.R.id.text1)).setText(listDevices.get(position).device.getName());
                return convertView;
            }
        };
        listView.setAdapter(listAdapter);

        bluetoothController.start();
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bluetoothController.stop();
    }

    @Override
    public void notAvailable() {
        Toast.makeText(getContext(), "Your device doesn't support bluetooth", Toast.LENGTH_SHORT).show();
        dismiss();
    }

    @Override
    public void connected(BluetoothSocket bluetoothSocket) {
        Toast.makeText(getContext(), "connected", Toast.LENGTH_SHORT).show();
        ((MyApp)getActivity().getApplication()).setBluetoothSocketAndStartConnection(bluetoothSocket);
    }

    @Override
    public void disconnected() {
        Toast.makeText(getContext(), "disconnected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateListDevice(ArrayList<DeviceData> listDevice) {
        listDevices.clear();
        listDevices.addAll(listDevice);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void addDevice(DeviceData device) {
        listDevices.add(device);
        listAdapter.notifyDataSetChanged();
    }

    @Override
    public void startConnecting() {
        setCancelable(false);
        btnFindDevices.setEnabled(false);
        listView.setEnabled(false);
    }

    @Override
    public void stopConnecting() {
        setCancelable(true);
        listView.setEnabled(true);
        btnFindDevices.setEnabled(true);
    }

}