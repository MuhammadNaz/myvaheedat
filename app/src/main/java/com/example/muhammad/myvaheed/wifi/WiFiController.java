package com.example.muhammad.myvaheed.wifi;


import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import com.example.muhammad.myvaheed.Debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by Muhammad on 19.12.2015.
 */
public class WiFiController implements WiFiState.IWiFiState {

    @Override
    public void connected() { //any connection
        if (isConnected) {
            Debug.logError("we are already connected!");
            iWiFiController.connected();
            return;
        }
        if (isConnecting) {
            if (isRightConnection(SSID)) {
                Debug.log("WE ARE CONNECTED!");
                isConnecting = false;
                isConnected = true;
                stopHopingInConnecting();
                onSuccess();
                iWiFiController.connected();
                return;
            } else {
                Debug.logError("another device connected!");
                disconnect();
                connecting();
            }
        }
    }

    @Override
    public void disconnected() { //any disconnection
        if (isConnecting) {
            connecting();
            return;
        }
        if (isConnected) {
            if (mayDisconnect) {
                removeFetchingDataEveryTime();
                isConnected = false;
                host = null;
                wiFiState.stopSession();
                iWiFiController.disconnected();
                return;
            }
            connect(SSID, PASSW);
        }
    }

    @Override
    public void enabled() {
        if (isConnecting) {
            connecting();
            return;
        }
    }

    @Override
    public void disabled() {
        if (isConnecting) {
            Debug.log("Trying connecting...");
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(true);
            return;
        }
        if (isConnected) {
            removeFetchingDataEveryTime();
            isConnected = false;
            host = null;
            wiFiState.stopSession();
            iWiFiController.disconnected();
            return;
        }
    }

    @Override
    public void peersChanged() {
        if (isConnected)
            return;
        if (isConnecting) {
            connecting();
        }
    }

    @Override
    public void scanResultChanged() {
        if (isConnected)
            return;
        if (isConnecting) {
            connecting();
        }
    }

    @Override
    public void thisDevicesStateChanged() {

    }

    public interface IWiFiController {
        void connected();

        void disconnected();

        void connecting();

        void requireToEnableWiFi();

        void getData(String data);

        void fail(String text);
    }

    private final String TAG = "WiFiController";

    private Context context;
    private WifiManager mManager;
    private volatile IWiFiController iWiFiController;
    private WiFiState wiFiState;
    private ThreadHopingInPeersUpdated threadHopingInConnecting;
    private Handler runUiThread;
    private volatile CommandSenderThread senderThread;

    private volatile boolean isConnecting = false;
    private volatile boolean isConnected = false;
    private volatile boolean isSending = false;
    private volatile boolean mayDisconnect = false;
    private volatile int connectionTry = 0;

    private String SSID;
    private String PASSW;
    private String host;
    private int port = 3512;

    public WiFiController(Context context, IWiFiController iWiFiController) {
        this.context = context;
        this.iWiFiController = iWiFiController;
        init();
    }

    public int getStatus() {
        Debug.log("getStatus");
        while (!wiFiState.isReady()) {
            Thread.yield();
        }
        if (isConnected)
            return WiFiState.CONNECTED;
        else if (isConnecting)
            return WiFiState.CONNECTING;
        return WiFiState.DISCONNECTED;
    }

    private void getHost() {
        if (!isConnected) {
            Debug.logError("getHost error, not connected!");
            return;
        }
        host = getApIpAddr(context);
        Debug.log("host = " + host);
    }

    private byte[] convert2Bytes(int hostAddress) {
        byte[] addressBytes = { (byte)(0xff & hostAddress),
                (byte)(0xff & (hostAddress >> 8)),
                (byte)(0xff & (hostAddress >> 16)),
                (byte)(0xff & (hostAddress >> 24)) };
        return addressBytes;
    }

    public String getApIpAddr(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        byte[] ipAddress = convert2Bytes(dhcpInfo.serverAddress);
        try {
            String apIpAddr = InetAddress.getByAddress(ipAddress).getHostAddress();
            return apIpAddr;
        } catch (UnknownHostException e) {
            Debug.logError("Can not convert IP address");
            e.printStackTrace();
        }
        return null;
    }

    /*if success = 0, failed = -1*/
    private int addNetwork(String SSID, String passw) {

        if (!mManager.isWifiEnabled()) {
            mManager.setWifiEnabled(true);
            while (mManager.getConfiguredNetworks() == null) {
                Thread.yield();
            }
        }

        for (WifiConfiguration conf : mManager.getConfiguredNetworks()) {
            if (conf.SSID != null && conf.SSID.equals(SSID))
                return 0;
        }
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + SSID + "\"";   // Please note the quotes. String should contain ssid in quotes
        conf.preSharedKey = "\"" + passw + "\"";
        return mManager.addNetwork(conf);
    }

    public void connect(String SSID, String password) {
        if (isConnecting) {
            Debug.logError("already trying connecting!");
            return;
        }
        while (!wiFiState.isReady()) {
            Debug.logError("wiFiState is not ready or WiFiManager is null");
            Thread.yield();
        }
        addNetwork(SSID, password);
        Debug.log("Connecting...");
        this.SSID = SSID;
        this.PASSW = password;
        isConnecting = true;
        isConnected = false;
        connectionTry = 0;
        iWiFiController.connecting();
        if (wiFiState.isDeviceConnected()) {
            if (isRightConnection(SSID)) {
                connected();
                return;
            }
            /*WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            wifiManager.disconnect();*/
            connecting();
            return;
        }
        if (wiFiState.isWiFiEnabled()) {
            connecting();
        } else {
            mManager.setWifiEnabled(true);
        }
    }

    private void connecting() {
        if (!isConnecting || isConnected) {
            Debug.logError("isConnecting = false or isConnected = true");
            return;
        }
        Debug.log("STARTING CONNECTING");
        beginHopingInConnecting();
        List<WifiConfiguration> list = mManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            Log.d("devices", "SSID = " + i.SSID);
            if (i.SSID != null && i.SSID.equals("\"" + SSID + "\"")) {
                Debug.log("wifiAP is known");
                if (!mManager.disconnect()) {
                    Debug.logError("can not disabled");
                    break;
                }
                if (!mManager.enableNetwork(i.networkId, true)) {
                    Debug.logError("can not enable network");
                    break;
                }

                if (!mManager.reconnect()) {
                    Debug.logError("can not reconnect");
                    break;
                }
                /*new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10 * 1000);
                            if (mManager.getConnectionInfo().getNetworkId() == -1) {
                                Debug.logError("connection is not available");
                                mManager.startScan();
                                beginHopingInConnecting();
                            }
                        } catch (InterruptedException e) {
                            *//*e.printStackTrace();
                            mManager.startScan();
                            beginHopingInConnecting();*//*
                        }
                    }
                }).start();*/
                return;
            }
        }
        //if not in conf list
        for (ScanResult scanResult : mManager.getScanResults()) {
            if (scanResult.SSID != null && scanResult.SSID.equals(SSID)) {
                Debug.log("wifiAP is new");
                if (addNetwork(SSID, PASSW) == -1) {
                    Debug.logError("error in adding network");
                    return;
                }
                connecting();
                return;
            }
        }
        Debug.logError("Can not scan that device");
        mManager.startScan();
    }

    private boolean isRightConnection(String SSID) {
        WifiInfo wifiInfo = mManager.getConnectionInfo();
        if (("\"" + SSID + "\"").equals(wifiInfo.getSSID()))
            return true;
        else
            Debug.logError("SSID of another device = " + wifiInfo.getSSID());
        return false;
    }

    private void disconnect() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.disconnect();
    }

    public void start() {
        Debug.log("start wifi controller");
        wiFiState.startSession();
    }

    public void release() {
        Debug.log("release wifi controller");
        isConnecting = false;
        mayDisconnect = true;
        //mManager.disconnect();
        mManager.setWifiEnabled(false);
    }

    private void init() {
        mManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wiFiState = new WiFiState(context, this);
        runUiThread = new Handler(context.getMainLooper());
    }

    public void onSuccess() {
        Debug.log("SUCCESS CONNECTION");
        getHost();
    }

    private void fail(String textError) {
        iWiFiController.fail(textError);
        release();
    }

    public void setFetchingDataEveryTime(long milliseconds) {
        if (!isConnected) {
            Debug.logError("setFetchingDataEveryTime error, not connected!");
            return;
        }
        if (milliseconds <= 0) {
            Debug.logError("setFetchingDataEveryTime error, milliseconds are not correct");
            return;
        }
        if (senderThread == null) {
            senderThread = new CommandSenderThread(context, milliseconds);
            senderThread.start();
        }
    }

    public void removeFetchingDataEveryTime() {
        if (senderThread != null) {
            senderThread.setStop();
            senderThread = null;
        }
    }

    public void fetchData(String message) {
        if (!isConnected) {
            Debug.logError("fetchData error, not connected!");
            return;
        }
        if (message != null && !message.equals(""))
            if (!isSending)
                new SendToEsp(port, message).start();
            else
                Debug.logError("fetchData can not, sending!");
    }

    public boolean isSetFethcingDataEveryTime() {
        return senderThread != null && senderThread.isAlive();
    }

    private void beginHopingInConnecting() {
        if (connectionTry == 20) {
            threadHopingInConnecting.interrupt();
            isConnecting = false;
            fail("Не удалось найти данное устройство");
            return;
        }
        stopHopingInConnecting();
        threadHopingInConnecting = new ThreadHopingInPeersUpdated();
        threadHopingInConnecting.start();
        connectionTry++;
    }

    private void stopHopingInConnecting() {
        if (threadHopingInConnecting != null)
            threadHopingInConnecting.interrupt();
    }

    private class CommandSenderThread extends Thread {
        private volatile boolean isStopped = false;
        private Context context;
        private long milliseconds;
        public CommandSenderThread(Context context, long milliseconds) {
            this.context = context;
            this.milliseconds = milliseconds;
        }
        @Override
        public void run() {
            super.run();
            while (!isStopped) {
                try {
                    Thread.sleep(milliseconds);
                    //sendCommand(context);
                    fetchData("get data");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }

        public void setStop() {
            isStopped = true;
            interrupt();
        }
    }


    private class SendToEsp extends Thread {
        private int port;
        private String message;
        private PrintWriter printWriter = null;
        private BufferedReader in = null;
        private final int BUFFER_SIZE = 1024;
        public SendToEsp(int port, String message) {
            this.port = port;
            this.message = message;
        }

        @Override
        public void run() {
            super.run();
            Debug.log("fetchData...");
            if (!isConnected)
                return;

            int waiting = 0;
            while (host == null) {
                Debug.logError("waiting for hostname");
                Thread.yield();
                waiting++;
                if (waiting >= 50) {
                    Debug.logError("hostname is null");
                    return;
                }
            }

            /*while (isSending) {
                Debug.logError("waiting for sending");
                Thread.yield();
            }*/

            isSending = true;
            Socket socket = new Socket();
            try {
                /**
                 * Create a client socket with the host,
                 * port, and timeout information.
                 */
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), 1500);
                printWriter = new PrintWriter(socket.getOutputStream());
                printWriter.write(message);
                printWriter.flush();
                try {
                    String message = "";
                    int charsRead = 0;
                    char[] buffer = new char[BUFFER_SIZE];

                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while ((charsRead = in.read(buffer)) != -1) {
                        message += new String(buffer).substring(0, charsRead);
                    }
                    iWiFiController.getData(message);
                } catch (IOException e) {
                    Debug.logError("Error receiving response:  " + e.getMessage());
                }

            } catch (IOException e) {
                //catch logic
                isSending = false;
            }

/**
 * Clean up any open sockets when done
 * transferring or if an exception occurred.
 */ finally {
                isSending = false;
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            if (in != null)
                                in.close();
                            if (printWriter != null)
                                printWriter.close();
                            socket.close();
                        } catch (IOException e) {
                            //catch logic
                        }
                    }
                }
            }
            isSending = false;
        }

    }

    private class ThreadHopingInPeersUpdated extends Thread {

        @Override
        public void run() {
            super.run();
            try {
                Debug.log("run hoping...");
                Thread.sleep(60 * 1000);
                if (!isConnected) {
                    Debug.logError("hoping is wasted!");
                    isConnecting = false;
                    runUiThread.post(new Runnable() {
                        @Override
                        public void run() {
                            fail("Не удалось подключиться");
                        }
                    });
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
