package com.example.muhammad.myvaheed.fragments;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.muhammad.myvaheed.R;
import com.example.muhammad.myvaheed.bluetooth.ConnectionThread;
import com.example.muhammad.myvaheed.bluetooth.MsgKV;
import com.example.muhammad.myvaheed.wifi.WiFiService;

/**
 * Created by Muhammad on 02.12.2015.
 */
public class TermoFragment extends Fragment implements ConnectToWiFiFragment.IGetDataFromWiFi{
    private double currentValue;
    private int levelOfBattery;
    private int maxValue;
    private int minValue;
    private final int MIN_VALUE = 10;
    private SeekBar sbMax;
    private SeekBar sbMin;
    private Switch switcher;
    private TextView tvCurrentValue;
    private TextView tvMinValue;
    private TextView tvMaxValue;
    private TextView tvLevelOfBattery;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.alarm);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //((MyApp) getActivity().getApplication()).getConnectionThread().addIConnectionThread(this);

        View view = inflater.inflate(R.layout.fragment_termo, container, false);
        tvCurrentValue = (TextView) view.findViewById(R.id.tvCurrentValue);
        tvMaxValue = (TextView) view.findViewById(R.id.tvMaxValue);
        tvMinValue = (TextView) view.findViewById(R.id.tvMinValue);
        tvLevelOfBattery = (TextView) view.findViewById(R.id.tvLevelOfBattery);
        switcher = (Switch) view.findViewById(R.id.switcher);
        sbMax = (SeekBar) view.findViewById(R.id.sbMax);
        sbMin = (SeekBar) view.findViewById(R.id.sbMin);

        sbMin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > maxValue)
                    sbMax.setProgress(progress);
                minValue = progress;
                tvMinValue.setText(String.valueOf(progress + MIN_VALUE));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbMax.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress < minValue)
                    sbMin.setProgress(progress);
                maxValue = progress;
                tvMaxValue.setText(String.valueOf(progress + MIN_VALUE));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                ((MyApp) getActivity().getApplication()).getConnectionThread().fetchData(null);
            }
        });
        sbMax.setProgress(30);
        sbMax.setProgress(20);

        return view;
    }



    private void temperatureGot(String number) {
        currentValue = Float.parseFloat(number);
        currentValue = Math.round(currentValue);
        if (tvCurrentValue != null && getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvCurrentValue.setText(String.valueOf(currentValue));
                }
            });
            checkSecurity();
        }
    }

    private void batteryGot(String number) {
        levelOfBattery = Integer.parseInt(number);
        if (tvLevelOfBattery != null && getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvLevelOfBattery.setText(String.valueOf(levelOfBattery) + "%");
                }
            });
    }

    public void checkSecurity() {
        if ((currentValue - MIN_VALUE) > minValue && (currentValue - MIN_VALUE) < maxValue)
            return;
        vibrator.vibrate(100);
        if (!mediaPlayer.isPlaying())
            mediaPlayer.start();
    }

    @Override
    public void getData(String msg) {
        temperatureGot(msg);
    }
}
