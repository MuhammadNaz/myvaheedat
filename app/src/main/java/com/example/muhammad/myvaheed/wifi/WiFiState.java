package com.example.muhammad.myvaheed.wifi;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import com.example.muhammad.myvaheed.Debug;

/**
 * Created by Muhammad on 19.12.2015.
 */
public class WiFiState {
    public interface IWiFiState {
        void connected();
        void disconnected();
        void enabled();
        void disabled();
        void peersChanged();
        void scanResultChanged();
        void thisDevicesStateChanged();
    }

    private volatile boolean isWiFiEnabled = false;
    private volatile boolean isDeviceConnected = false;
    private volatile boolean isRunningSession = false;
    private Context context;
    private IWiFiState iWiFiState;
    private BroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private static final String ACTION_CONNECTED = "DEVICE_CONNECTED";
    private volatile boolean isUndefinedStatus = true;

    public final static int CONNECTED = 0;
    public final static int DISCONNECTED = 1;
    public final static int CONNECTING = 2;
    public final static int UNDEFINED = 3;

    private WiFiState() {}

    public WiFiState(Context context, IWiFiState iWiFiState) {
        this.context = context;
        this.iWiFiState = iWiFiState;
        init();
    }

    public void startSession() {
        if (isRunningSession) return;
        isRunningSession = true;
        context.registerReceiver(mReceiver, mIntentFilter);
    }

    public boolean isReady() {
        return !isUndefinedStatus;
    }

    public void stopSession() {
        if (!isRunningSession) return;
        isRunningSession = false;
        context.unregisterReceiver(mReceiver);
    }

    private void init() {
        mReceiver = new WiFiDirectBroadcastReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        //mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        mIntentFilter.addAction(ACTION_CONNECTED);
    }

    public boolean isWiFiEnabled() {
        return isWiFiEnabled;
    }

    public boolean isDeviceConnected() {
        return isDeviceConnected;
    }

    public static class WifiReceiver extends BroadcastReceiver {

        public WifiReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {

            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (info != null && info.isConnected()) {
                // e.g. To check the Network Name or other info:
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                String ssid = wifiInfo.getSSID();
                Debug.log("SSID from receiver = " + ssid);
                Intent connectionChanged = new Intent();
                connectionChanged.setAction(ACTION_CONNECTED);
                //PendingIntent pi = PendingIntent.getBroadcast(context, 0,  intent, PendingIntent.FLAG_UPDATE_CURRENT);
                context.sendBroadcast(connectionChanged);
            }
        }
    }

    public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

        public WiFiDirectBroadcastReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                // Check to see if Wi-Fi is enabled and notify appropriate activity
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                    // Wifi P2P is enabled
                    Debug.log("onenabled");
                    isWiFiEnabled = true;
                    iWiFiState.enabled();
                } else {
                    // Wi-Fi P2P is not enabled
                    Debug.log("ondisabled");
                    isWiFiEnabled = false;
                    iWiFiState.disabled();
                }
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // Call WifiP2pManager.requestPeers() to get a list of current peers
                Debug.log("onpeerschanged");
                iWiFiState.peersChanged();
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action) || ACTION_CONNECTED.equals(action)) {
                // Respond to new connection or disconnections
                Debug.log("connection changed");
                WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifi.getConnectionInfo();
                SupplicantState suplState = wifiInfo.getSupplicantState();
                Debug.log("state = " + suplState.toString());
                if (suplState == SupplicantState.COMPLETED) {
                    ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                    if (mWifi.isConnected()) {
                        Debug.log("onconnected");
                        isDeviceConnected = true;
                        iWiFiState.connected();
                    } else {
                        Debug.log("ondisconnected");
                        isDeviceConnected = false;
                        iWiFiState.disconnected();
                    }
                }
                isUndefinedStatus = false;
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                // Respond to this device's wifi state changing
                Debug.log("this device changed");
                WifiManager wifi = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wifiInfo = wifi.getConnectionInfo();
                SupplicantState suplState = wifiInfo.getSupplicantState();
                Debug.log("state = " + suplState.toString());
                iWiFiState.thisDevicesStateChanged();
            } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
                Debug.log("onscanresultchanged");
                iWiFiState.scanResultChanged();
            }
        }
    }
}
