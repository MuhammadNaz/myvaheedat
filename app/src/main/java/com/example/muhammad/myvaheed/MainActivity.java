package com.example.muhammad.myvaheed;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.muhammad.myvaheed.fragments.ButtonLifeFragment;
import com.example.muhammad.myvaheed.fragments.ConnectToWiFiFragment;
import com.example.muhammad.myvaheed.fragments.HeartSensorFragment;
import com.example.muhammad.myvaheed.fragments.MyDialogFragment;
import com.example.muhammad.myvaheed.fragments.SensorsFragment;
import com.example.muhammad.myvaheed.fragments.TermoFragment;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    ViewPager viewPager;
    DialogFragment dialogFragment;
    ViewPagerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        viewPager = (ViewPager) findViewById(R.id.main_container);

        if (savedInstanceState == null) {
            adapter = new ViewPagerAdapter(fragmentManager);
            viewPager.setAdapter(adapter);
            viewPager.setOffscreenPageLimit(4);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.saveDataSerializable(User.getInstance(), User.FILE_PATH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_devices) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("devices");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);
            dialogFragment = new MyDialogFragment();
            dialogFragment.show(fragmentManager, "devices");
            return true;
        }

        if (id == R.id.action_addnumbers) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View v = getLayoutInflater().inflate(R.layout.alertdialog_addnumbers,null, false);
            final ListView lvNumbers = (ListView) v.findViewById(R.id.lvPhoneNumbers);
            lvNumbers.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, User.getInstance().getPhoneNumbers()));
            ((Button) v.findViewById(R.id.btnAddNumbers)).setOnClickListener(new View.OnClickListener() {
                ListView listView = lvNumbers;
                AlertDialog.Builder builder;
                Dialog dialog;
                @Override
                public void onClick(View v) {
                    final EditText editText = new EditText(MainActivity.this);
                    builder = new AlertDialog.Builder(MainActivity.this);
                    dialog = builder.setView(editText).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog1, int which) {
                            String phone = editText.getText().toString();
                            if (!phone.matches("[+,0-9]+")) {
                                Toast.makeText(MainActivity.this, "Set a real phone number", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                                dialog.show();
                                return;
                            }
                            User.getInstance().addNumber(phone);
                            Toast.makeText(MainActivity.this, "Added", Toast.LENGTH_SHORT).show();
                            ((ArrayAdapter<String>) listView.getAdapter()).notifyDataSetChanged();
                        }
                    }).create();
                    dialog.show();
                }
            });
            builder.setView(v).setTitle("Phone numbers").show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static class ViewPagerAdapter extends FragmentStatePagerAdapter {
        SensorsFragment mainFragment;
        ButtonLifeFragment buttonLifeFragment;
        TermoFragment termoFragment;
        HeartSensorFragment heartSensorFragment;
        ConnectToWiFiFragment connectToWiFiFragment;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            connectToWiFiFragment = new ConnectToWiFiFragment();
            heartSensorFragment = new HeartSensorFragment();
            termoFragment = new TermoFragment();
            connectToWiFiFragment.addIGetDataListener(heartSensorFragment);
            connectToWiFiFragment.addIGetDataListener(termoFragment);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    /*buttonLifeFragment = new ButtonLifeFragment();
                    return buttonLifeFragment;*/
                    return connectToWiFiFragment;
                case 1:
                    return heartSensorFragment;
                case 2:
                    return termoFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
