package com.example.muhammad.myvaheed.wifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;

import com.example.muhammad.myvaheed.Debug;

import java.util.ArrayList;

/**
 * Created by Muhammad on 20.12.2015.
 */
public class WiFiFragment extends Fragment{
    private ArrayList<IGetDataFromWiFi> listIGetDataFromWifi = new ArrayList<>();
    protected volatile boolean isConnected;
    public interface IGetDataFromWiFi {
        void getData(String data);
    }

    @Override
     public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverFromService);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiverFromService, new IntentFilter(WiFiService.ACTION_RECEIVER));
        getStatus(); //for update UI
        isSetFetchingDataEveryTime();
    }

    public void addIGetDataListener(IGetDataFromWiFi listener) {
        listIGetDataFromWifi.add(listener);
    }

    protected void connect() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.CONNECT_FLAG);
        getActivity().startService(intent);
    }

    protected void disconnect() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.RELEASE_FLAG);
        getActivity().startService(intent);
    }

    protected void getStatus() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.GET_STATUS_FLAG);
        getActivity().startService(intent);
    }

    protected void isSetFetchingDataEveryTime() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.IS_SET_FETCH_EVERY_TIME_FLAG);
        getActivity().startService(intent);
    }

    protected void onConnected() {
        isConnected = true;
    }

    protected void onDisconnected() {
        isConnected = false;
    }

    protected void onConnecting() {

    }

    protected void onIsSetFetchingDataEveryTime(boolean isSet) {

    }

    protected void setFetchingDataEveryTime(long milliseconds) {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.SET_FETCH_DATA_EVERY_TIME_FLAG);
        intent.putExtra(WiFiService.KEY_SET_FETCH_DATA_EVERY_TIME_FLAG, milliseconds);
        getActivity().startService(intent);
    }

    protected void removeFetchingDtataEveryTime() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.REMOVE_FETCH_DATA_EVERY_TIME_FLAG);
        getActivity().startService(intent);
    }

    protected void fetchData() {
        Intent intent = new Intent(getActivity(), WiFiService.class);
        intent.putExtra(WiFiService.KEY_CONTROLLER, WiFiService.FETCH_DATA_FLAG);
        getActivity().startService(intent);
    }

    private BroadcastReceiver receiverFromService = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            if (intent.hasExtra(WiFiService.KEY_WI_FI_STATUS)) {
                int connectionState = intent.getIntExtra(WiFiService.KEY_WI_FI_STATUS, -1);
                if (connectionState == -1)
                    return;

                Debug.log("RECEIVED KEY_WI_FI_STATUS");
                switch (connectionState) {
                    case WiFiState.CONNECTED:
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onConnected();
                            }
                        });
                        break;
                    case WiFiState.DISCONNECTED:
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onDisconnected();
                            }
                        });
                        break;
                    case WiFiState.CONNECTING:
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                onConnecting();
                            }
                        });
                        break;
                }
            } else if (intent.hasExtra(WiFiService.KEY_GET_DATA)) {
                Debug.log("RECEIVED KEY_GET_DATA");
                for (int i = 0; i < listIGetDataFromWifi.size(); i++) {
                    listIGetDataFromWifi.get(i).getData(intent.getStringExtra(WiFiService.KEY_GET_DATA));
                }
            } else if (intent.hasExtra(WiFiService.KEY_IS_SET_FETCH_DATA_EVERY_TIME)) {
                Debug.log("RECEIVED IS_SET_FETCH_DATA_EVERY_TIME");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onIsSetFetchingDataEveryTime(intent.getBooleanExtra(WiFiService.KEY_IS_SET_FETCH_DATA_EVERY_TIME, false));
                    }
                });
            }
        }
    };
}
