package com.example.muhammad.myvaheed.wifi;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.muhammad.myvaheed.*;


/**
 * Created by Muhammad on 19.12.2015.
 */
public class WiFiService extends Service implements WiFiController.IWiFiController{
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private WiFiController wiFiController;
    private SharedPreferences preferences;

    public final static String KEY_CONTROLLER = "KYE_CONTROLLER";
    public final static String KEY_WI_FI_STATUS = "KYE_STATUS";
    public final static String KEY_GET_DATA = "KYE_GET_DATA";
    public final static String KEY_IS_SET_FETCH_DATA_EVERY_TIME = "KEY_IS_SET_FETCH_DATA_EVERY_TIME";
    public final static String KEY_SET_FETCH_DATA_EVERY_TIME_FLAG = "KEY_SET_FETCH_DATA_EVERY_TIME_FLAG";

    public final static String ACTION_RECEIVER = "ACTION_RECEIVER_FROM_WIFI_SERVICE_MYVAHEED";

    public final static int CONNECT_FLAG = 0;
    public final static int RELEASE_FLAG = 1;
    public final static int GET_STATUS_FLAG = 2;
    public final static int SEND_COMMAND_FLAG = 3;
    public final static int SET_FETCH_DATA_EVERY_TIME_FLAG = 4;
    public final static int REMOVE_FETCH_DATA_EVERY_TIME_FLAG = 5;
    public final static int FETCH_DATA_FLAG = 6;
    public final static int IS_SET_FETCH_EVERY_TIME_FLAG = 7;
    public final static int RECEIVER_CODE_CONNECTED = 0;
    public final static int RECEIVER_CODE_DISCONNECTED = 1;
    public final static int RECEIVER_CODE_CONNECTING = 2;

    /*private final static String MY_SSID = "ESPap2";
    private final static String MY_PASSW = "12345678";*/

    private final static String MY_SSID = "Matiz";
    private final static String MY_PASSW = "XRuT87r6hA";

    private volatile boolean isKilled = false;
    private final int REESTABLISH_FLAG  = -123;
    private final String NAME_PREFERENCES = "WiFiService";
    private final String REESTABLISH_KEY_IS_SET_FETCHING_EVERY_TIME = "REESTABLISH_KEY_IS_SET_FETCHING_EVERY_TIME";
    private final String REESTABLISH_KEY_SET_FETCHING_EVERY_TIME = "REESTABLISH_KEY_SET_FETCHING_EVERY_TIME";


    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(NAME_PREFERENCES, MODE_PRIVATE);
    }

    @Override
    public void connected() {
        Toast.makeText(this, "Подключено!", Toast.LENGTH_SHORT).show();
        mySendBroadcast(KEY_WI_FI_STATUS, WiFiState.CONNECTED);
        if (isKilled) {
            reestablish();
        }
        isKilled = false;
    }

    private void reestablish() {
        Debug.log("Reestablishing...");
        Intent reestablishIntent = new Intent();
        if (preferences.getBoolean(REESTABLISH_KEY_IS_SET_FETCHING_EVERY_TIME, false)) {
            reestablishIntent.putExtra(KEY_CONTROLLER, SET_FETCH_DATA_EVERY_TIME_FLAG);
            reestablishIntent.putExtra(KEY_SET_FETCH_DATA_EVERY_TIME_FLAG, preferences.getLong(REESTABLISH_KEY_SET_FETCHING_EVERY_TIME, -1));
            onStartCommand(reestablishIntent, REESTABLISH_FLAG, -1);
        }
        //if...
    }

    @Override
    public void disconnected() {
        Toast.makeText(this, "WiFi-связь отключена", Toast.LENGTH_SHORT).show();
        mySendBroadcast(KEY_WI_FI_STATUS, WiFiState.DISCONNECTED);
        stopSelf();
    }

    @Override
    public void connecting() {
        mySendBroadcast(KEY_WI_FI_STATUS, WiFiState.CONNECTING);
    }

    private void mySendBroadcast(String action, Object data) {
        Intent intent = new Intent(ACTION_RECEIVER);
        if (KEY_WI_FI_STATUS.equals(action)) {
            intent.putExtra(KEY_WI_FI_STATUS, (Integer) data);
        } else
        if (KEY_GET_DATA.equals(action)) {
            intent.putExtra(KEY_GET_DATA, (String) data);
        }
        if (KEY_IS_SET_FETCH_DATA_EVERY_TIME.equals(action)) {
            intent.putExtra(KEY_IS_SET_FETCH_DATA_EVERY_TIME, (Boolean) data);
        }
        sendBroadcast(intent);
    }

    @Override
    public void requireToEnableWiFi() {

    }

    @Override
    public void getData(String data){
        Debug.log("data = " + data);
        mySendBroadcast(KEY_GET_DATA, data);
    }

    @Override
    public void fail(String text) {
        Toast.makeText(this, "Ошибка при соединении: " + text, Toast.LENGTH_SHORT).show();
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            if (wiFiController == null) {
                Debug.log("Creating wifiController");
                wiFiController = new WiFiController(getApplicationContext(), WiFiService.this);
                wiFiController.start();
            }
            SharedPreferences.Editor editor = preferences.edit();
            switch (msg.arg1) {
                case CONNECT_FLAG:
                    Debug.log("CONNECT FLAG");
                    wiFiController.connect(MY_SSID, MY_PASSW);
                    break;
                case RELEASE_FLAG:
                    Debug.log("DISCONNECT FLAG");
                    wiFiController.release();
                    getLooper().quit();
                    wiFiController = null;
                    break;
                case GET_STATUS_FLAG:
                    Debug.log("GET STATUS FLAG");
                    mySendBroadcast(KEY_WI_FI_STATUS, wiFiController.getStatus());
                    break;
               /* case SEND_COMMAND_FLAG:
                    Debug.log("SEND COMMAND FLAG");
                    String text = (String) msg.obj;
                    wiFiController.fetchData(text);
                    break;*/
                case SET_FETCH_DATA_EVERY_TIME_FLAG:
                    Debug.log("SET FETCH DATA EVERY TIME FLAG");
                    long milliseconds = (Long) msg.obj;
                    editor.putBoolean(REESTABLISH_KEY_IS_SET_FETCHING_EVERY_TIME, true);
                    editor.putLong(REESTABLISH_KEY_SET_FETCHING_EVERY_TIME, milliseconds);
                    editor.commit();
                    wiFiController.setFetchingDataEveryTime(milliseconds);
                    break;
                case REMOVE_FETCH_DATA_EVERY_TIME_FLAG:
                    Debug.log("REMOVE FETCH DATA EVERY TIME FLAG");
                    editor.putBoolean(REESTABLISH_KEY_IS_SET_FETCHING_EVERY_TIME, false);
                    editor.commit();
                    wiFiController.removeFetchingDataEveryTime();
                    break;
                case FETCH_DATA_FLAG:
                    Debug.log("SET FETCH DATA FLAG");
                    wiFiController.fetchData("get data from esp");
                    break;
                case IS_SET_FETCH_EVERY_TIME_FLAG:
                    Debug.log("IS SET FETCH EVERY TIME FLAG");
                    mySendBroadcast(KEY_IS_SET_FETCH_DATA_EVERY_TIME, wiFiController.isSetFethcingDataEveryTime());
                    break;
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mServiceLooper == null) {
            Debug.log("create thread");
            HandlerThread thread = new HandlerThread("WiFiThreadMyvaheed", Process.THREAD_PRIORITY_BACKGROUND);
            thread.start();

            // Get the HandlerThread's Looper and use it for our Handler
            mServiceLooper = thread.getLooper();
            mServiceHandler = new ServiceHandler(mServiceLooper);
        }

        if (intent == null) { //killed
            Debug.logError("Service is killed...");
            intent = new Intent();
            intent.putExtra(KEY_CONTROLLER, CONNECT_FLAG);
            isKilled = true;
        } else
            if (flags != REESTABLISH_FLAG)
                isKilled = false;

        int flag = intent.getIntExtra(KEY_CONTROLLER, -1);
        if (flag == -1) {
            Debug.logError("ERROR IN STARTING SERVICE: FLAG IS EMPTY");
            stopSelf();
        }

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = flag;

        /*if (flag == SEND_COMMAND_FLAG) //get data regarding command
            msg.obj = intent.getStringExtra(KEY_GET_DATA);*/
        if (flag == SET_FETCH_DATA_EVERY_TIME_FLAG)
            msg.obj = intent.getLongExtra(KEY_SET_FETCH_DATA_EVERY_TIME_FLAG, -1);
        mServiceHandler.sendMessage(msg);
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Debug.log("WiFi onDestroy");
        super.onDestroy();
        if (mServiceLooper != null) {
            mServiceLooper = null;
        }
    }
}
