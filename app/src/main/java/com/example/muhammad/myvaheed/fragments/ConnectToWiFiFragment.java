package com.example.muhammad.myvaheed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.muhammad.myvaheed.R;
import com.example.muhammad.myvaheed.wifi.WiFiFragment;

/**
 * Created by Muhammad on 21.12.2015.
 */
public class ConnectToWiFiFragment extends WiFiFragment {
    private TextView tvConnectionStatus;
    private Button btnConnection;
    private Switch swAutofetching;
    private Button btnFetchData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_to_wifi_fragment, container, false);
        tvConnectionStatus = (TextView) view.findViewById(R.id.tvConnectionStatus);
        btnConnection = (Button) view.findViewById(R.id.btnConnection);
        btnFetchData = (Button) view.findViewById(R.id.btnFetchData);
        swAutofetching = (Switch) view.findViewById(R.id.switchAutoFetch);
        swAutofetching.setOnCheckedChangeListener(listenerOfSwitch);

        btnConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isConnected) {
                    connect();
                } else {
                    disconnect();
                }
            }
        });

        btnFetchData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchData();
            }
        });

        btnConnection.setEnabled(false);
        btnConnection.setClickable(false);
        btnFetchData.setEnabled(false);
        btnFetchData.setClickable(false);
        btnConnection.setText("Undefined");
        tvConnectionStatus.setText("Undefined");
        return view;
    }

    @Override
    protected void onConnected() {
        super.onConnected();
        tvConnectionStatus.setText(R.string.connected);
        btnConnection.setEnabled(true);
        btnConnection.setClickable(true);
        btnConnection.setText(R.string.disconnect);
        btnFetchData.setEnabled(true);
        btnFetchData.setClickable(true);
        if (swAutofetching.isChecked()) {
            setFetchingDataEveryTime(2 * 60 * 1000);
        }
    }

    @Override
    protected void onConnecting() {
        super.onConnecting();
        tvConnectionStatus.setText(R.string.connecting);
        btnConnection.setText(R.string.connect);
        btnConnection.setEnabled(false);
        btnConnection.setClickable(false);
        btnFetchData.setEnabled(false);
        btnFetchData.setEnabled(false);
    }

    @Override
    protected void onDisconnected() {
        super.onDisconnected();
        tvConnectionStatus.setText(R.string.disconnected);
        btnConnection.setEnabled(true);
        btnConnection.setClickable(true);
        btnFetchData.setEnabled(false);
        btnFetchData.setEnabled(false);
        btnConnection.setText(R.string.connect);
    }

    @Override
    protected void onIsSetFetchingDataEveryTime(boolean isSet) {
        super.onIsSetFetchingDataEveryTime(isSet);
        swAutofetching.setOnCheckedChangeListener(null);
        swAutofetching.setChecked(isSet); //only update UI
        swAutofetching.setOnCheckedChangeListener(listenerOfSwitch);
    }

    private CompoundButton.OnCheckedChangeListener listenerOfSwitch = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (!isConnected)
                return;
            if (isChecked) {
                setFetchingDataEveryTime(2 * 60 * 1000);
            } else {
                removeFetchingDtataEveryTime();
            }
        }
    };
}
