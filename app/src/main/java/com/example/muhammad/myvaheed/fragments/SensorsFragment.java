package com.example.muhammad.myvaheed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.muhammad.myvaheed.R;

/**
 * Created by Muhammad on 24.09.2015.
 */
public class SensorsFragment extends Fragment {

    private static final String TAG_HEARTFRAGMENT = "heartfragment";
    private FragmentManager fragmentManager;
    private HeartSensorFragment heartSensorFragment;
    private TermoFragment termoFragment;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getFragmentManager();
        heartSensorFragment = new HeartSensorFragment();
        termoFragment = new TermoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sensors, container, false);
        fragmentManager.beginTransaction().add(R.id.frlFirstContainer, heartSensorFragment, TAG_HEARTFRAGMENT).commit();
        fragmentManager.beginTransaction().add(R.id.frlSecondContainer, termoFragment, TAG_HEARTFRAGMENT).commit();
        return v;
    }
}
