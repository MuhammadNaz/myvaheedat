package com.example.muhammad.myvaheed;

import android.app.Application;
import android.bluetooth.BluetoothSocket;

import com.example.muhammad.myvaheed.bluetooth.ConnectionThread;


/**
 * Created by AbduLlah on 07.05.2015.
 */
public class MyApp extends Application {

    private BluetoothSocket bluetoothSocket;
    public static String TAG = "Myvaheed";
    private ConnectionThread connectionThread = new ConnectionThread();
    public static String FILEDIR_PATH;

    @Override
    public void onCreate() {
        super.onCreate();
        FILEDIR_PATH = getFilesDir().getAbsolutePath();
    }

    public void setBluetoothSocketAndStartConnection(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
        connectionThread.setBluetoothSocket(bluetoothSocket);
        connectionThread.start();
    }

    public BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public ConnectionThread getConnectionThread() {
        return connectionThread;
    }
}

