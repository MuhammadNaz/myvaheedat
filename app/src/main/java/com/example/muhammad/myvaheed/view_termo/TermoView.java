package com.example.muhammad.myvaheed.view_termo;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.example.muhammad.myvaheed.Utils;

/**
 * Created by Muhammad on 02.12.2015.
 */
public class TermoView extends SurfaceView {
    private static final String TAG = "TermoView";
    private int maxTermo = 40;
    private int minTermo = 19;
    private int currentTermo;

    private int mainLineSize;
    private int separateLineSize;
    private int thrLineSize;



    public TermoView(Context context) {
        super(context);
    }

    class DinamicGraphThread extends Thread {
        private volatile boolean isStopped = false;
        private volatile boolean isSuspend = false;
        private SurfaceHolder surfaceHolder;
        public DinamicGraphThread(SurfaceHolder holder) {
            surfaceHolder = holder;
        }

        public boolean isStopping() {
            return isStopped;
        }


        public void setStopping(boolean isStopping) {
            this.isStopped = isStopping;
        }


        public boolean isSuspend() {
            return isSuspend;
        }


        public void setSuspend(boolean isSuspend) {
            this.isSuspend = isSuspend;
        }


        @Override
        public void run() {
            Canvas canvas = null;
            // TODO Auto-generated method stub
            Utils.log("begin thread");
            while(!isStopped) {
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) { //� ��������, ������� �������� � VGView ����� �������� ������ ���� �����
                        //drawGraph(canvas);
                    }
                }
                finally {
                    if (canvas != null)
                        surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            Utils.log("end thread");
        }

    }
}
