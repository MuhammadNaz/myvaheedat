package com.example.muhammad.myvaheed.bluetooth;

import android.bluetooth.BluetoothDevice;

/**
 * Created by AbduLlah on 08.05.2015.
 */
public class DeviceData {
    public BluetoothDevice device;
    public boolean isPaired;
}
