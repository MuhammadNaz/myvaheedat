package com.example.muhammad.myvaheed.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.muhammad.myvaheed.MyApp;
import com.example.muhammad.myvaheed.R;
import com.example.muhammad.myvaheed.TimeFrameLeftThread;
import com.example.muhammad.myvaheed.User;
import com.example.muhammad.myvaheed.Utils;
import com.example.muhammad.myvaheed.bluetooth.ConnectionThread;
import com.example.muhammad.myvaheed.bluetooth.MsgKV;

import java.util.LinkedList;

import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Muhammad on 24.09.2015.
 */
public class HeartSensorFragment extends Fragment implements TimeFrameLeftThread.ITimeFrameLeft, ConnectToWiFiFragment.IGetDataFromWiFi {
    private LineChartView lineChartView;
    private LineChartData lineChartData;
    private Line line;
    private int numberOfPoints = 100;
    private int frameTime;
    private float score;
    private volatile boolean isTransacting;
    private Thread timeFrameLeftThread;
    private TimeFrameLeftThread runnableTimeThread;

    public void togglePause() {
        runnableTimeThread.togglePause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timeFrameLeftThread.interrupt();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        lineChartView = (LineChartView) inflater.inflate(R.layout.fragment_chart, container, false);
        lineChartView.setOnValueTouchListener(new LineChartOnValueSelectListener() {

            @Override
            public void onValueDeselected() {

            }

            @Override
            public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
                Toast.makeText(getContext(), String.valueOf(value.getY()), Toast.LENGTH_SHORT).show();
            }
        });
        lineChartView.setViewportCalculationEnabled(false);

        lineChartData = new LineChartData();
        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);
        axisX.setName("Axis X");
        axisY.setName("Axis Y");
        lineChartData.setAxisXBottom(axisX);
        lineChartData.setAxisYLeft(axisY);
        lineChartData.setBaseValue(Float.NEGATIVE_INFINITY);
        lineChartView.setZoomEnabled(false);
        lineChartView.setLineChartData(lineChartData);

        line = new Line();
        line.setColor(ChartUtils.COLORS[0]);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(true);
        line.setFilled(true);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(true);
        line.setHasLines(true);
        line.setHasPoints(false);

        LinkedList<Line> lines = new LinkedList<>();
        lines.add(line);
        lineChartData.setLines(lines);
        runnableTimeThread = new TimeFrameLeftThread(this);
        timeFrameLeftThread = new Thread(runnableTimeThread);
        timeFrameLeftThread.start();

        return lineChartView;
    }

    @Override
    public void onTimeFrameLeft() {
        frameTime++;
        addNode(isTransacting && score != 0);
        updateViewport();
        isTransacting = false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void reset() {
        line.getValues().clear();
        frameTime = 0;
        updateViewport();
    }

    private void updateViewport() {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(lineChartView.getMaximumViewport());
        v.bottom = 0;
        v.top = 60;
        v.left = frameTime - numberOfPoints/2;
        v.right = frameTime + numberOfPoints - 1;
        lineChartView.setMaximumViewport(v);
        lineChartView.setCurrentViewport(v);
    }

    private void addNode(boolean hasSignal) {
        PointValue value;
       /* if (hasSignal)
            value = new PointValue(frameTime, score++);
        else
        if (score != 0)
            value = new PointValue(frameTime, score--);
        else
            value = new PointValue(frameTime, 0);*/

        /*if (hasSignal)
            value = new PointValue(frameTime, score);
        else
            value = new PointValue(frameTime, 0);*/
        value = new PointValue(frameTime, score);

        synchronized (line) {
            //lineChartData.getLines().get(0).getValues().add(value);
            line.getValues().add(value);
            if (line.getValues().size() > 50)
                line.getValues().remove(0);
            lineChartView.setLineChartData(lineChartData);
        }
    }


    StringBuilder stringBuilder = new StringBuilder();
    @Override
    public void getData(String msg) {
        Log.e("wqer", "Signal = " + msg);
        isTransacting = true;
        score = Float.parseFloat(msg);
    }
}