package com.example.muhammad.myvaheed.bluetooth.acceptor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.widget.Toast;


import com.example.muhammad.myvaheed.bluetooth.ConnectionThread;
import com.example.muhammad.myvaheed.Utils;
import com.example.muhammad.myvaheed.bluetooth.BluetoothState;

import java.io.IOException;
import java.util.UUID;


/**
 * Created by AbduLlah on 08.05.2015.
 */
public class BluetoothController implements BluetoothState.IBluetoothStateListener{
    private Context context;
    private IBluetoothController listener;
    private BluetoothState bluetoothSetUp;
    private boolean isEnable;
    private final int DURATION_OF_DISCOVERABILITY = 60;
    private Handler runUIThread;
    private volatile boolean isDiscoverable = false;
    private AcceptConnection threadAcceptConnection;
    private final UUID MY_UUID = UUID.fromString("5a0f6c44-f622-11e4-b9b2-1697f925ec7b");

    public static final String TAG = "BtController";

    public interface IBluetoothController {
        void notAvailable();
        void connected(BluetoothSocket bluetoothSocket);
        void disconnected();
        void startAction();
        void stopAction();
    }

    public BluetoothController(Context context, IBluetoothController listener) {
        this.context = context;
        this.listener = listener;
        bluetoothSetUp = new BluetoothState(context, this);
        runUIThread =  new Handler(context.getMainLooper());
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void start() {
        Utils.log(TAG, "start");
        bluetoothSetUp.startSession();
        if (BluetoothAdapter.getDefaultAdapter() == null)
            if (listener != null)
                listener.notAvailable();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            isEnable = false;
            inquireOnBt();
        } else {
            isEnable = true;
            if (BluetoothAdapter.getDefaultAdapter().getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                if (listener != null)
                    listener.startAction();
            } else
                if (listener != null)
                    listener.stopAction();
            listenConnection();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
        context.registerReceiver(scanStateReceiver, intentFilter);
    }

    public void stop() {
        Utils.log(TAG, "stop");
        bluetoothSetUp.stopSession();
        stopListenConnection();
        context.unregisterReceiver(scanStateReceiver);
    }

    private void inquireOnBt() {
        Utils.log(TAG, "to turn on");
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        context.startActivity(enableBtIntent);
    }

    public void showMe() {
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DURATION_OF_DISCOVERABILITY);
        context.startActivity(discoverableIntent);
    }

    private final BroadcastReceiver scanStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, -1) == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                    listener.startAction();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, -1) == BluetoothAdapter.SCAN_MODE_CONNECTABLE) {
                    listener.stopAction();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE, -1) == BluetoothAdapter.SCAN_MODE_NONE) {
                    listener.stopAction();
                }
            }
        }
    };


    private class AcceptConnection extends Thread {

        private BluetoothServerSocket mServerSocket = null;
        private String name = "Child";

        public AcceptConnection() {
            try {
                mServerSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord(name, MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            BluetoothSocket bluetoothSocket = null;
            while(true) {
                try {
                    Utils.log(TAG, "listenConnection");
                    bluetoothSocket = mServerSocket.accept();
                } catch (IOException e) {
                    Utils.log(TAG, "Error in connecting");
                    e.printStackTrace();
                    break;
                }
                if (bluetoothSocket != null) {
                    final BluetoothSocket finalBluetoothSocket = bluetoothSocket;
                    runUIThread.post(new Runnable() {
                        @Override
                        public void run() {
                            Utils.log(TAG, "isConnected = " + finalBluetoothSocket.isConnected());
                            ConnectionThread.isConnected = true;
                            if (listener != null)
                                listener.connected(finalBluetoothSocket);
                        }
                    });

                    try {
                        mServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        public void cancel() {
            Utils.log(TAG, "stopListenConnection");
            if (mServerSocket != null) {
                try {
                    mServerSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void connected(BluetoothSocket bluetoothSocket) {
        runUIThread.post(new Runnable() {
            @Override
            public void run() {
                stop();
                start();
                Toast.makeText(context, "connected", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void listenConnection() {
        stopListenConnection(); //ensure a creating new socket - SUPERNEW!!!
        threadAcceptConnection = new AcceptConnection();
        threadAcceptConnection.start();
    }

    private void stopListenConnection() {
        if (threadAcceptConnection != null)
            threadAcceptConnection.cancel();
    }

    @Override
    public void on() {
        Utils.log(TAG, "bt on");
        isEnable = true;
        listenConnection();
    }

    @Override
    public void off() {
        Utils.log(TAG, "bt off");
        isEnable = false;
        inquireOnBt();
        stopListenConnection();
    }

    /*@Override
    public void connecting() {
        Utils.log(TAG, "bt connecting");
        if (listener != null)
            listener.startConnecting();
    }

    @Override
    public void disconnecting() {
        Utils.log(TAG, "bt disconnecting");
        if (listener != null)
            listener.startConnecting();
    }

    @Override
    public void connected() {
        Utils.log(TAG, "bt connected");
        if (listener != null) {
            listener.stopConnecting();
            listener.connected();
        }
    }

    @Override
    public void disconnected() {
        Utils.log(TAG, "bt disconnected");
        if (listener != null) {
            listener.stopConnecting();
            listener.disconnected();
            stop();
            start();
        }
    }*/
}
