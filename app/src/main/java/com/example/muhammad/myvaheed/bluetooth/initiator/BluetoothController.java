package com.example.muhammad.myvaheed.bluetooth.initiator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;


import com.example.muhammad.myvaheed.bluetooth.ConnectionThread;
import com.example.muhammad.myvaheed.Utils;
import com.example.muhammad.myvaheed.bluetooth.BluetoothState;
import com.example.muhammad.myvaheed.bluetooth.DeviceData;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * Created by AbduLlah on 08.05.2015.
 */
public class BluetoothController implements BluetoothState.IBluetoothStateListener {
    private Context context;
    private IBluetoothController listener;
    private BluetoothState bluetoothSetUp;
    private boolean isEnable;
    private CreateConnection createConnection;
    //private final UUID MY_UUID = UUID.fromString("5a0f6c44-f622-11e4-b9b2-1697f925ec7b");
    private final static UUID SerialPort_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private Handler runUIThread;
    public static final String TAG = "BtController";

    public interface IBluetoothController {
        void notAvailable();
        void connected(BluetoothSocket bluetoothSocket);
        void disconnected();
        void updateListDevice(ArrayList<DeviceData> listDevice);
        void addDevice(DeviceData device);
        void startConnecting();
        void stopConnecting();
    }

    public BluetoothController(Context context, IBluetoothController listener) {
        this.context = context;
        this.listener = listener;
        bluetoothSetUp = new BluetoothState(context, this);
        runUIThread = new Handler(context.getMainLooper());
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void start() {
        Utils.log(TAG, "start");
        bluetoothSetUp.startSession();
        if (BluetoothAdapter.getDefaultAdapter() == null)
            if (listener != null)
                listener.notAvailable();
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            isEnable = false;
            inquireOnBt();
        } else {
            isEnable = true;
            getPairedDevices();
            if (BluetoothAdapter.getDefaultAdapter().isDiscovering())
                if (listener != null)
                    listener.startConnecting();
            else
                if (listener != null)
                    listener.stopConnecting();
        }
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(receiverDiscoveringDevices, filter);
    }

    public void stop() {
        Utils.log(TAG, "stop");
        bluetoothSetUp.stopSession();
        context.unregisterReceiver(receiverDiscoveringDevices);
    }

    private void inquireOnBt() {
        Utils.log(TAG, "to turn on");
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        context.startActivity(enableBtIntent);
    }

    private void getPairedDevices() {
        Utils.log(TAG, "getParedDevices");
        Set<BluetoothDevice> pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        // If there are paired devices
        Utils.log(TAG, "pairedDev size = " + pairedDevices.size());
        ArrayList<DeviceData> listDevice = new ArrayList<>();
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice btDevice : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                DeviceData device = new DeviceData();
                device.device = btDevice;
                device.isPaired = true;
                listDevice.add(device);
            }
        }
        if (listener != null)
            listener.updateListDevice(listDevice);
    }

    public void discoverDevices() {
        Utils.log(TAG, "disccoverDevices");
        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().startDiscovery();
        }
        else
            inquireOnBt();
    }

    public void unpairDevice(final BluetoothDevice device) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Method m = device.getClass()
                            .getMethod("removeBond", (Class[]) null);
                    m.invoke(device, (Object[]) null);
                    Thread.sleep(1000);
                    runUIThread.post(new Runnable() {
                        @Override
                        public void run() {
                            getPairedDevices();
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }).start();
    }

    private final BroadcastReceiver receiverDiscoveringDevices = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                DeviceData deviceData = new DeviceData();
                deviceData.device = device;
                deviceData.isPaired = false;
                listener.addDevice(deviceData);
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                if (listener != null)
                    listener.startConnecting();
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (listener != null)
                    listener.stopConnecting();
            }
        }
    };


    @Override
    public void on() {
        Utils.log(TAG, "bt on");
        isEnable = true;
        getPairedDevices();
    }

    @Override
    public void off() {
        Utils.log(TAG, "bt off");
        isEnable = false;
        inquireOnBt();
    }

/*
    @Override
    public void connecting() {
        Utils.log(TAG, "bt connecting");
        if (listener != null)
            listener.startConnecting();
    }

    @Override
    public void disconnecting() {
        Utils.log(TAG, "bt disconnecting");
        if (listener != null)
            listener.startConnecting();
    }

    @Override
    public void connected() {
        Utils.log(TAG, "bt connected");
        if (listener != null) {
            listener.stopConnecting();
            listener.connected();
        }
    }

    @Override
    public void disconnected() {
        Utils.log(TAG, "bt disconnected");
        if (listener != null) {
            listener.stopConnecting();
            listener.disconnected();
            stop();
            start();
        }
    }
*/

    public void connect(BluetoothDevice device) {
        if (createConnection != null)
            createConnection.cancel();
        createConnection = new CreateConnection(device);
        createConnection.start();
    }


    private class CreateConnection extends Thread {
        private BluetoothSocket bluetoothSocket;
        private BluetoothSocket tmp;

        private boolean mAllowInsecureConnections = true;
        public CreateConnection(BluetoothDevice bluetoothDevice) {
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                if ( mAllowInsecureConnections ) {
                    Method method;
                    method = bluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[] { int.class } );
                    tmp = (BluetoothSocket) method.invoke(bluetoothDevice, 1);
                }
                else {
                    tmp = bluetoothDevice.createRfcommSocketToServiceRecord(SerialPort_UUID);
                }
            } catch (Exception e) {
                Log.e(TAG, "create() failed", e);
            }

            bluetoothSocket = tmp;
        }

        @Override
        public void run() {
            while (true) {
                BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                try {
                    Utils.log(TAG, "connect");
                    bluetoothSocket.connect();
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        bluetoothSocket.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    return;
                }
                ConnectionThread.isConnected = true;
                runUIThread.post(new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            Utils.log(TAG, "BTController = " + bluetoothSocket.isConnected());
                            listener.connected(bluetoothSocket);
                        }
                    }
                });
                break;
            }
        }

        public void cancel() {
            Utils.log(TAG, "cancel");
            ConnectionThread.isConnected = false;
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
