package com.example.muhammad.myvaheed.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.muhammad.myvaheed.R;
import com.example.muhammad.myvaheed.User;
import com.example.muhammad.myvaheed.Utils;

/**
 * Created by Muhammad on 24.09.2015.
 */
public class ButtonLifeFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_buttonlife, container, false);
        ((Button) v.findViewById(R.id.btnLife)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Help", Toast.LENGTH_SHORT).show();
                for (String phone : User.getInstance().getPhoneNumbers()) {
                    //Utils.sendSMS(getContext(), phone, "HELP!");
                }
            }
        });
        return v;
    }
}
