package com.example.muhammad.myvaheed.bluetooth;

import java.io.Serializable;

/**
 * Created by AbduLlah on 10.05.2015.
 */
public class MsgKV implements Serializable {

    private static final long serialVersionUID = -1344895903;

    public MsgKV(int type) {
        this.type = type;
    }

    public MsgKV() {

    }
    public int type;
    public static final int TYPE_SIGNAL = 1;
    public String object = "null"; // not null!!!
}