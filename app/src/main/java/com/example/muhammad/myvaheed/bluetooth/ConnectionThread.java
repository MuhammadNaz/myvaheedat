
package com.example.muhammad.myvaheed.bluetooth;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.example.muhammad.myvaheed.Utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;


/**
 * Created by AbduLlah on 17.05.2015.
 */
public class ConnectionThread extends Thread {

    public static String TAG = "ConnectionThread";

    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private int dataLength;
    private BluetoothSocket bluetoothSocket;
    private ArrayList<IConnectionThread> listIConnectionThread = new ArrayList<>();
    public static volatile boolean isConnected = false;

    public interface IConnectionThread {
        void getData(String msg);
        void getData(MsgKV msgKV);
        void connected();
        void disconnected();
    }

    public ConnectionThread() {}

    public ConnectionThread(BluetoothSocket bluetoothSocket) {
        setBluetoothSocket(bluetoothSocket);
    }

    public void setBluetoothSocket(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
        Log.e(TAG, "isConnected = " + bluetoothSocket.isConnected());
        try {
            inputStream = bluetoothSocket.getInputStream();
            outputStream = bluetoothSocket.getOutputStream();
            for (int i = 0; i < listIConnectionThread.size(); i++) {
                listIConnectionThread.get(i).connected();
            }
        } catch (IOException e) {
            Utils.log(TAG, "error construct connection thread");
            e.printStackTrace();
        }
    }

    public void addIConnectionThread(IConnectionThread iConnectionThread) {
        listIConnectionThread.add(iConnectionThread);
    }

    public void removeIConnectionThread(IConnectionThread iConnectionThread) {
        listIConnectionThread.remove(iConnectionThread);
    }

    @Override
    public void run() {
        ByteArrayInputStream inputByteStream = null;
        ObjectInputStream in = null;
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                dataLength = inputStream.read(buffer);
                if (dataLength == -1)
                    continue;
                Utils.log(TAG, "read");
                String readMessage = new String(buffer, 0, dataLength);
                /*inputByteStream = new ByteArrayInputStream(buffer); for object type
                in = new ObjectInputStream(inputByteStream);
                MsgKV msgKV = null;
                //msgKV = (MsgKV) in.readObject();*/
                for (int i = 0; i < listIConnectionThread.size(); i++) {
                    listIConnectionThread.get(i).getData(readMessage);
                }
                /*in.close();
                inputByteStream.close();*/
            } catch (IOException e) {
                Utils.log(TAG, "Connection lost");
                close();
                for (int i = 0; i < listIConnectionThread.size(); i++) {
                    listIConnectionThread.get(i).disconnected();
                }
                e.printStackTrace();
                break;
            } /*catch (ClassNotFoundException e) {
                Utils.log(TAG, "ClassNotFound");
                if (iConnectionThread != null)
                    iConnectionThread.disconnected();
                e.printStackTrace();
                break;
            }*/
        }
    }


    public void write(MsgKV msgKV) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        Utils.log(TAG, "write");
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(msgKV);
            byte[] buffer = bos.toByteArray();
            outputStream.write(buffer);
            out.close();
            bos.close();
        } catch (IOException e) {
            Utils.log(TAG, "error write e " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void write(String msg) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        Utils.log(TAG, "write");
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(msg.getBytes());
            byte[] buffer = bos.toByteArray();
            outputStream.write(buffer);
            out.close();
            bos.close();
        } catch (IOException e) {
            Utils.log(TAG, "error write e " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void close() {
        Utils.log(TAG, "close");
        isConnected = false;
        try {
            bluetoothSocket.close();
        } catch (IOException e) {
            Utils.log(TAG, "error close");
            e.printStackTrace();
        }
    }
}
