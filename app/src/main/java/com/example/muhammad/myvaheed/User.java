package com.example.muhammad.myvaheed;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Muhammad on 27.09.2015.
 */
public class User implements Serializable {
    private static User user = null;
    private List<String> phoneNumbers;
    public static final String FILE_PATH = MyApp.FILEDIR_PATH + "/user";

    public static User getInstance() {
        if (user == null)
            user = (User) Utils.loadDataSerializable(FILE_PATH);
        if (user == null)
            user = new User();
        return user;
    }

    private User() {
        phoneNumbers = new ArrayList<>();
    }

    public void addNumber(String phone) {
        phoneNumbers.add(phone);
    }

    public ArrayList<String> getPhoneNumbers() {
        return (ArrayList<String>) phoneNumbers;
    }
}
