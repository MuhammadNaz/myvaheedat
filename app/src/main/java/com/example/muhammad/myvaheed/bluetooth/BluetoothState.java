package com.example.muhammad.myvaheed.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by AbduLlah on 08.05.2015.
 */
public class BluetoothState {

    private Context context;
    private BluetoothAdapter bluetoothAdapter;
    private IBluetoothStateListener listener;

    public interface IBluetoothStateListener {
        void on();
        void off();
        /*void connecting();
        void disconnecting();
        void connected();
        void disconnected();*/
    }

    public BluetoothState(Context context, IBluetoothStateListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void startSession() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(receiverBluetooth, intentFilter);
    }

    public void stopSession() {
        context.unregisterReceiver(receiverBluetooth);
    }

    private BroadcastReceiver receiverBluetooth = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_ON) {
                    if (listener != null)
                        listener.on();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_OFF) {
                    if (listener != null)
                        listener.off();
                }
               /* if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_CONNECTING) {
                    if (listener != null)
                        listener.connecting();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_DISCONNECTING) {
                    if (listener != null)
                        listener.disconnecting();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_DISCONNECTED) {
                    if (listener != null)
                        listener.disconnected();
                }
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_CONNECTED) {
                    if (listener != null)
                        listener.connected();
                }*/
            }
        }
    };
}
