package com.example.muhammad.myvaheed;

/**
 * Created by Muhammad on 24.09.2015.
 */
public class TimeFrameLeftThread implements Runnable {
    private ITimeFrameLeft listener;
    private final int TIME_FRAME = 50;
    private volatile boolean isPause = false;
    private TimeFrameLeftThread() {

    }
    public TimeFrameLeftThread(ITimeFrameLeft listener) {
        this.listener = listener;
    }

    public interface ITimeFrameLeft {
        void onTimeFrameLeft();
    }

    public void togglePause() {
        this.isPause = !isPause;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(TIME_FRAME);
                if (!isPause)
                    listener.onTimeFrameLeft();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}