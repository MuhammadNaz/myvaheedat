package com.visualgraph.kursach;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.visualgraph.kursach.Graph.Edge;
import com.visualgraph.kursach.Graph.Node;
import com.visualgraph.kursach.anim.VisualAlgthms.SequenceStep;

public class VGView extends SurfaceView implements SurfaceHolder.Callback {

	private Graph graph;
	private DinamicGraphThread dinamicGraph; //����� ��� ����������� "�������������" ����������� ����� � ������� ������ VGView
	private ArrayList<Node> listGraph;
	private SequenceStep steps;
	
	public static float screenWidth;
	public static float screenHeight;
	private int txtSize;
	private int arrowSize;
	private int lineWidth;
	
	//������� ���������
	private Point borderRadiusEnd;
	private Point borderRadiusBegin;
	private Paint txtNameNodePaint;
	private Paint txtWghtEdgePain	t;
	private Paint lnFirstArrow;
	private Paint lnScndArrow;
	private Paint lnEdgePaint;
	private Paint clNodePaint;
	private Paint clNodePaintWhite;
	private Paint backgroundPaint;
	private Paint txtAlgDijkstra;
	public static boolean IS_PAINTING = false;
	
	public VGView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		graph = Graph.getInstance();
		getHolder().addCallback(this);
		listGraph = graph.getListGraph();
		borderRadiusEnd = new Point();
		borderRadiusBegin = new Point();
		
		txtAlgDijkstra = new Paint();
		txtNameNodePaint = new Paint();
		txtWghtEdgePaint = new Paint();
		lnFirstArrow = new Paint();
		lnScndArrow = new Paint();
		clNodePaint = new Paint();
		clNodePaintWhite = new Paint();
		lnEdgePaint = new Paint();
		backgroundPaint = new Paint();
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		screenHeight = h;
		screenWidth = w;
		Node.RADIUS = w / 15;
		lineWidth = w / 150;
		arrowSize = Node.RADIUS / 2;
		txtSize = (int) (Node.RADIUS * 0.5);
		txtNameNodePaint.setTextSize(txtSize);
		txtWghtEdgePaint.setTextSize((float) (txtSize * 2));
		txtAlgDijkstra.setTextSize((float) (txtSize*1.2));
		txtNameNodePaint.setAntiAlias(true);
		txtWghtEdgePaint.setAntiAlias(true);
		txtAlgDijkstra.setAntiAlias(true);
		txtWghtEdgePaint.setColor(Color.RED);
		txtAlgDijkstra.setColor(Color.RED);
		lnEdgePaint.setStrokeWidth(lineWidth);
		lnFirstArrow.setStrokeWidth(lineWidth);
		lnScndArrow.setStrokeWidth(lineWidth);
		backgroundPaint.setColor(Color.WHITE);
		clNodePaintWhite.setColor(Color.WHITE);
	}
	
	
	
	public void drawGraph(Canvas canvas) {
		
	try { //� ��� ����� ��������� ������, ��� ��������� ������ � ������, ��� ���, ����� drawGraph ���������� ���������
		canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundPaint); //������� �����
		for (int i = 0; i < listGraph.size(); i++) {
			float x = listGraph.get(i).x;
			float y = listGraph.get(i).y;
			ArrayList<Edge> edges = listGraph.get(i).getEdges();
			
			//blue = ����� ���-��� + ���������� ������, green = ���������� ������, black = �� ���������� ������ � �� � �������� ���.
			if (listGraph.get(i).isSelect() || VisualGraphActivity.REZHIM_VISUAL) {
				if (listGraph.get(i).isSelect() && VisualGraphActivity.REZHIM_VISUAL) {
					clNodePaint.setColor(Color.BLUE);
				} else
					clNodePaint.setColor(Color.GREEN);
			}
			else {
				clNodePaint.setColor(Color.BLACK);
			}
			canvas.drawCircle(x, y, Node.RADIUS, clNodePaint);
			canvas.drawCircle(x, y, (float) (Node.RADIUS * 0.9), clNodePaintWhite);
			canvas.drawText(listGraph.get(i).getName(), x, y, txtNameNodePaint);
			if (listGraph.get(i).isVisit()) { 
				if (listGraph.get(i).getDistance() == Graph.INF)
					canvas.drawText("Inf", x, y + Node.RADIUS, txtAlgDijkstra);
				else if (listGraph.get(i).getDistance() != 0) //������� �������� �� ����������
					canvas.drawText(String.valueOf(listGraph.get(i).getDistance()), x, y + Node.RADIUS, txtAlgDijkstra);
			}
			
			//������ ����� �������
			for (int j = 0; j < edges.size(); j++) {
				Edge edge = edges.get(j);
				float length = (float) Math.sqrt(Math.pow(edge.getBegin().x - edge.getEnd().x, 2) + 
						Math.pow(edge.getBegin().y - edge.getEnd().y, 2));
				//��� ��������� ������� ���������������� �����
				float cosInlectionOfEdge;
				float sinInlectionOfEdge;
				
				cosInlectionOfEdge = (edge.getEnd().x - edge.getBegin().x) / length;
				sinInlectionOfEdge = (edge.getEnd().y - edge.getBegin().y) / length;
				
				borderRadiusEnd.x = (int) (edge.getEnd().x - Node.RADIUS*cosInlectionOfEdge);
				borderRadiusEnd.y = (int) (edge.getEnd().y - Node.RADIUS*sinInlectionOfEdge);
				
				borderRadiusBegin.x = (int) (edge.getBegin().x + Node.RADIUS*cosInlectionOfEdge);
				borderRadiusBegin.y = (int) (edge.getBegin().y + Node.RADIUS*sinInlectionOfEdge);
				
				if (steps != null) {
					if (edge.isSelect()) {
						lnEdgePaint.setColor(Color.BLUE);
					} else {
						lnEdgePaint.setColor(Color.BLACK);
					}
				}
				
				canvas.drawLine(borderRadiusBegin.x, borderRadiusBegin.y, borderRadiusEnd.x, borderRadiusEnd.y, lnEdgePaint);
				
				if (edge.isOrgraph()) {
					double angleSin = Math.asin(sinInlectionOfEdge);
					double angleCos = Math.acos(cosInlectionOfEdge);
					
					if (cosInlectionOfEdge * sinInlectionOfEdge > 0) {
						angleSin += Math.toRadians(10);
						angleCos += Math.toRadians(10);
						canvas.drawLine(
								(float) (borderRadiusEnd.x - arrowSize
										* Math.cos(angleCos)),
								(float) (borderRadiusEnd.y - arrowSize
										* Math.sin(angleSin)),
								(float) borderRadiusEnd.x,
								(float) borderRadiusEnd.y, lnEdgePaint);
						angleSin -= Math.toRadians(20);
						angleCos -= Math.toRadians(20);
						canvas.drawLine(
								(float) (borderRadiusEnd.x - arrowSize
										* Math.cos(angleCos)),
								(float) (borderRadiusEnd.y - arrowSize
										* Math.sin(angleSin)),
								(float) borderRadiusEnd.x,
								(float) borderRadiusEnd.y, lnEdgePaint);
					}
					else {
						angleSin += Math.toRadians(10);
						angleCos -= Math.toRadians(10);
						canvas.drawLine(
								(float) (borderRadiusEnd.x - arrowSize
										* Math.cos(angleCos)),
								(float) (borderRadiusEnd.y - arrowSize
										* Math.sin(angleSin)),
								(float) borderRadiusEnd.x,
								(float) borderRadiusEnd.y, lnEdgePaint);
						angleSin -= Math.toRadians(20);
						angleCos += Math.toRadians(20);
						canvas.drawLine(
								(float) (borderRadiusEnd.x - arrowSize
										* Math.cos(angleCos)),
								(float) (borderRadiusEnd.y - arrowSize
										* Math.sin(angleSin)),
								(float) borderRadiusEnd.x,
								(float) borderRadiusEnd.y, lnEdgePaint);
					}
				}
				if (edge.getWeight() != 0) {
					canvas.drawText(String.valueOf(edge.getWeight()), (borderRadiusBegin.x + borderRadiusEnd.x)/2, (borderRadiusBegin.y + borderRadiusEnd.y)/2, txtWghtEdgePaint);
				}
				
			}
		}
		} catch (Exception e) {
			VGTest.log("������ ��� ��������� " + e.getMessage());
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		startDinamicGraph(steps);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		stopDinamicGraph();
	}
	
	public void stopDinamicGraph() {
		boolean retry = true;
		if (dinamicGraph != null && IS_PAINTING)
			dinamicGraph.setStopping(true);
		 while (retry) //����������� ��������� ������
	      {
	         try
	         {
	            dinamicGraph.join();
	            retry = false;
	         } // end try
	         catch (InterruptedException e)
	         {
	         } // end catch
	      }
		 IS_PAINTING = false;
	}
	
	public void startDinamicGraph(SequenceStep steps) {
		if ((dinamicGraph == null || dinamicGraph.isStopping()) && !IS_PAINTING) {
			IS_PAINTING = true;
			dinamicGraph = new DinamicGraphThread(getHolder());
			dinamicGraph.start();
		}
		this.steps = steps;
	}
	
	class DinamicGraphThread extends Thread {
		private volatile boolean isStopped = false;
		private volatile boolean isSuspend = false;
		private SurfaceHolder surfaceHolder;
		public DinamicGraphThread(SurfaceHolder holder) {
			surfaceHolder = holder;
		}
		
		public boolean isStopping() {
			return isStopped;
		}


		public void setStopping(boolean isStopping) {
			this.isStopped = isStopping;
		}


		public boolean isSuspend() {
			return isSuspend;
		}


		public void setSuspend(boolean isSuspend) {
			this.isSuspend = isSuspend;
		}


		@Override
		public void run() {
			Canvas canvas = null;
			// TODO Auto-generated method stub
			VGTest.log("����� ������");
			while(!isStopped) {
				try {
					canvas = surfaceHolder.lockCanvas(null);
					synchronized (surfaceHolder) { //� ��������, ������� �������� � VGView ����� �������� ������ ���� �����
						drawGraph(canvas);
					}
				}
				finally {
					if (canvas != null) 
						surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
			VGTest.log("����� ����������");
		}
		
	}
}
